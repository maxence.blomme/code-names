﻿import asyncio
import re

from asgiref.sync import sync_to_async

from backend.models import Card, Game, Player
from backend.serializers import CardSerializer, GameSerializer
from backend.utils.clue_manager import empty_clues
from backend.utils.player_manager import delete_players
from backend.utils.word_manager import delete_words
from backend.utils.game_states import GameStateEnum
from backend.utils.redis import Redis


def valid_game_id(game_id):
    return 0 < len(game_id) <= 32 and re.match(r"^[a-zA-Z0-9]+$", game_id)


def game_exists(game_id):
    return len(Game.objects.filter(game_id=game_id)) > 0


def get_game_cards(game_id, serialized=False):
    cards = Card.objects.filter(game_id=game_id).order_by('id')
    if serialized:
        return CardSerializer(cards, many=True).data
    else:
        return cards


def get_game(game_id, serialized=False):
    game = Game.objects.filter(game_id=game_id)[0]
    if serialized:
        return GameSerializer(game).data
    else:
        return game


def delete_game(game_id):
    Card.objects.filter(game_id=game_id).delete()
    Game.objects.filter(game_id=game_id).delete()
    empty_clues(game_id)


def clean_game(game_id):
    delete_game(game_id)
    delete_players(game_id)
    delete_words(game_id)


def db_reveal_word(game_id, word):
    queryset = Card.objects.filter(game_id=game_id, word=word)
    if len(queryset) == 0 or queryset[0].revealed:
        return None
    queryset.update(revealed=True)
    return queryset[0]


def decrement_blue(game):
    current = game.remaining_blue
    Game.objects.filter(game_id=game.game_id).update(remaining_blue=current-1)
    return current <= 1


def decrement_red(game):
    current = game.remaining_red
    Game.objects.filter(game_id=game.game_id).update(remaining_red=current-1)
    return current <= 1


def set_win(game_id, blue_won):
    Game.objects.filter(game_id=game_id).update(state=GameStateEnum.FINISHED.value, blue_won=blue_won)


def set_state(game_id, state):
    Game.objects.filter(game_id=game_id).update(state=state.value)


def is_private(game_id):
    return Game.objects.filter(game_id=game_id)[0].private


def game_theme(game_id):
    return Game.objects.filter(game_id=game_id)[0].theme


def game_sec_theme(game_id):
    return Game.objects.filter(game_id=game_id)[0].sec_theme


def game_combine(game_id):
    return Game.objects.filter(game_id=game_id)[0].combine


def join(game_id):
    Game.objects.filter(game_id=game_id).update(joined=True)


def get_player_count(game_id):
    return len(Player.objects.filter(game_id=game_id).exclude(color="spec"))


def set_prop_deleting_game(game_id):
    Redis().set("deleting-" + game_id, 1)


def get_prop_deleting_game(game_id):
    return Redis().get("deleting-" + game_id)


def del_prop_deleting_game(game_id):
    Redis().delete("deleting-" + game_id)


async def start_game_deletion_delay(game_id):
    await asyncio.sleep(10)
    if not get_prop_deleting_game(game_id):
        return
    await asyncio.sleep(600)
    if get_prop_deleting_game(game_id):
        await sync_to_async(clean_game)(game_id)
