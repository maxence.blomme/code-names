import random

from backend.models import CustomWord, Word


def generate_word_map(game_id, theme, custom_words, combine, sec_theme):
    if theme == "custom":
        words_main = generate_custom_words(game_id, custom_words)
    else:
        words_main = generate_words(theme)

    if combine:
        words_sec = generate_words(sec_theme)
        return combine_words(words_main, words_sec)

    return words_main


def generate_words(theme):
    words = Word.objects.filter(theme=theme)
    if len(words) == 0:
        words = Word.objects.filter(theme="default")
    return list(map(lambda w: w.word, random.sample(list(words), k=25)))


def generate_custom_words(game_id, custom_words):
    if len(CustomWord.objects.filter(game_id=game_id)) == 0:
        add_custom_words(game_id, custom_words)
    words = CustomWord.objects.filter(game_id=game_id)
    return list(map(lambda w: w.word, random.sample(list(words), k=25)))


def add_custom_words(game_id, custom_words):
    for word in custom_words:
        CustomWord.objects.create(game_id=game_id, word=word)


def combine_words(words_main, words_sec):
    words = []

    for i in range(len(words_sec)):  # words_main may be shorter if the game uses custom words
        if i >= len(words_main):
            words.append(words_sec[i])
        else:
            words.append(words_main[i] if random.randint(0, 1) else words_sec[i])

    return words


def delete_words(game_id):
    CustomWord.objects.filter(game_id=game_id).delete()
