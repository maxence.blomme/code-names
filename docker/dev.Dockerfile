FROM node:13-alpine AS npm

WORKDIR /react

ADD web/frontend/react/package.json /react/package.json

RUN npm install

ADD web/frontend/react/ /react

RUN npm run build


FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=code_names.settings_dev

ENV REDIS_HOST=redis
ENV REDIS_PORT=6379

RUN mkdir /code_names

WORKDIR /code_names

RUN apk add --update --no-cache --virtual .tmp \
        gcc           \
        musl-dev      \
        alpine-sdk    \
        libffi-dev    \
        libressl-dev  \
        openssl-dev
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ADD web/requirements.txt /code_names/requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN apk del .tmp

ADD web/ /code_names/
RUN rm -rf /code_names/frontend/react

COPY --from=npm /react/build/ /code_names/frontend/react/build/

CMD python manage.py migrate && python populate_database.py words 1 && python manage.py runserver 0.0.0.0:65000
