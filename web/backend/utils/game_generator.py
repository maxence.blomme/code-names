﻿import random

from backend.models import Word, Card, Game
from backend.utils.game_states import GameStateEnum
from backend.utils.word_manager import generate_word_map


def generate_game(game_id, private, theme, custom_words, combine, sec_theme):
    words = generate_word_map(game_id, theme, custom_words, combine, sec_theme)

    color_map, blue_starts = generate_color_map()

    create_cards(game_id, words, color_map)
    Game.objects.create(game_id=game_id,
                        blue_starting=blue_starts,
                        remaining_blue=(9 if blue_starts else 8),
                        remaining_red=(8 if blue_starts else 9),
                        state=(GameStateEnum.BLUE_SPY_TURN.value if blue_starts else GameStateEnum.RED_SPY_TURN.value),
                        theme=theme,
                        combine=combine,
                        sec_theme=sec_theme,
                        private=private)


def generate_color_map():
    blue_starts = random.randint(0, 1)
    color_map = ["black"] + ["red"] * 8 + ["blue"] * 8 + ["neutral"] * 7 + \
                (["blue"] if blue_starts else ["red"])
    random.shuffle(color_map)
    return color_map, blue_starts


def create_cards(game_id, words, color_map):
    for i in range(len(words)):
        Card.objects.create(game_id=game_id, word=words[i], color=color_map[i])
