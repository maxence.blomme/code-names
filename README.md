# Code Names

Jeu CodeNames en ligne avec Django et React.js

## Lancement

### Développement

La configuration Docker de développement fait appel à une base de données SQLite locale
pour plus de simplicité. Une configuration Docker Compose est fournie pour lancer l'application
de développement avec son serveur Redis dédié :
```bash
docker-compose up
```
Le serveur se lance alors par défaut sur le port 65000 de la machine.

### Production

3 images Docker de production sont incluses dans le dossier `docker/` selon le type de système de
gestion de bases de données à utiliser entre :
- SQLite
- MySQL
- PostgreSQL

La configuration de connexion des conteneurs de production passe par des variables d'environnement :
```
## Communes à toutes les images
CODENAMES_HOST  # Nom de domaine autorisé pour l'application (known_hosts dans les settings Django)
REDIS_HOST      # Hôte du serveur Redis à utiliser
REDIS_PORT      # Port du serveur Redis à utiliser

## Pour l'image PostgreSQL
POSTGRES_HOST      # Hôte du serveur PostgreSQL à utiliser
POSTGRES_PORT      # Port du serveur PostgreSQL à utiliser
POSTGRES_DB        # Nom de la base de données de l'application
POSTGRES_USER      # Nom d'utilisateur PostgreSQL
POSTGRES_PASSWORD  # Mot de passe PostgreSQL

## Pour l'image MySQL
MYSQL_CNF  # Chemin vers le fichier de configuration MySQL à utiliser (à monter comme volume dans le conteneur)
```

Les images Docker de production sont disponibles publiquement dans leur dernière version sous les tags suivants :
```
## Images x86
maxenceblomme/code-names:sqlite
maxenceblomme/code-names:postgres
maxenceblomme/code-names:mysql

## Images ARM
maxenceblomme/code-names:sqlite-arm
maxenceblomme/code-names:postgres-arm
maxenceblomme/code-names:mysql-arm
```

## Mots jouables

La base de données créée par le projet est de base vide. Il faut donc y insérer les
mots jouables pour que l'application fonctionne. Le script Python `populate_database.py`
permet de la peupler automatiquement.

Ce script prend en argument le chemin vers le fichier contenant les mots, chaque mot devant être séparé
des autres par un caractère quelconque n'étant pas une lettre. Ainsi, un fichier
nommé `mots` dont le contenu est le suivant :
```
MOT1,MOT2,MOT3,...
```
peut peupler la base de données via la commande suivante :
```bash
python populate_database.py mots
```
Un deuxième argument optionnel permet de dire au script de peupler une base SQLite ou PostgreSQL au lieu d'une
base MySQL. Pour cela, il suffit de rajouter un second argument :
```bash
python populate_database.py mots 1  # Utiliser SQLite
python populate_database.py mots 2  # Utiliser PostgreSQL
```

Le fichier `words` contient 683 mots francais, ils peuvent etre utilisés pour
peupler la base de mots du jeu. On peut donc exécuter la commande suivante pour le faire :
```bash
python populate_database.py words {,0,1,2}
```
