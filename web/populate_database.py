#!/usr/bin/python

import os, sys, re, sqlite3
from enum import Enum

try:
    import MySQLdb
except:
    pass
try:
    import psycopg2
except:
    pass

class DbTypeEnum(Enum):
    MYSQL = "0"
    SQLITE = "1"
    POSTGRES = "2"

try:
    sys.argv[1]
except IndexError:
    print("Error - missing input directory name! Usage ./populate_database.py input_directory")
    sys.exit(1)
else:
    words_dir = sys.argv[1]

db_type = DbTypeEnum.MYSQL
try:
    sys.argv[2]
except IndexError:
    pass
else:
    db_type = DbTypeEnum(sys.argv[2])

# Check on whether file exists and is accessible
if not os.path.isdir(words_dir):
    print("Error - input directory", words_dir, "is not accessible!")
    sys.exit(1)

if db_type == DbTypeEnum.SQLITE:
    conn = sqlite3.connect('./db.sqlite3')
elif db_type == DbTypeEnum.MYSQL:
    conn = MySQLdb.connect(read_default_file=os.environ['MYSQL_CNF'])
elif db_type == DbTypeEnum.POSTGRES:
    conn = psycopg2.connect("host='" + os.environ['POSTGRES_HOST'] + "' " +
                            "port='" + os.environ['POSTGRES_PORT'] + "' " +
                            "dbname='" + os.environ['POSTGRES_DB'] + "' " +
                            "user='" + os.environ['POSTGRES_USER'] + "' " +
                            "password='" + os.environ['POSTGRES_PASSWORD'] + "'");

cursor = conn.cursor()

cursor.execute('DELETE FROM backend_word')

count = 0
for filename in os.listdir(words_dir):
    f = open(os.path.join(words_dir, filename), "r", encoding='utf8')
    for line in f:
        for word in re.findall(r"[\w\-]+", line):
            if db_type == DbTypeEnum.SQLITE:
                cursor.execute('INSERT INTO backend_word (word,theme) VALUES (?,?)', (word, filename,))
            else:
                cursor.execute('INSERT INTO backend_word (word,theme) VALUES (%s,%s)', (word, filename,))
            count += 1

conn.commit()

print(count, "words added")
f.close()
