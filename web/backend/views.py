from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from rest_framework import mixins, status

from .models import Game, Word
from .serializers import CreateGameSerializer, GameSerializer, ThemeSerializer
from .utils.game_generator import generate_game
from .utils.game_manager import game_exists, valid_game_id, clean_game, get_player_count


class CreateGame(mixins.CreateModelMixin,
                 GenericAPIView):
    game_id = ''
    serializer_class = CreateGameSerializer

    def post(self, request, *args, **kwargs):
        game_id = request.data.get('game_id', '')
        private = request.data.get('private', False)
        private = False if private == '' else private
        theme = request.data.get('theme', 'default')
        custom_words = request.data.get('custom', [])
        combine = request.data.get('combine', False)
        sec_theme = request.data.get('sec_theme', '')

        if not valid_game_id(game_id):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        elif game_exists(game_id):
            return Response(status=status.HTTP_200_OK)

        generate_game(game_id, private, theme, custom_words, combine, sec_theme)

        return Response(status=status.HTTP_201_CREATED)


class Themes(mixins.ListModelMixin,
             GenericAPIView):
    serializer_class = ThemeSerializer
    queryset = Word.objects.values('theme').distinct()

    def get(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class PublicGameList(mixins.ListModelMixin,
                     GenericAPIView):
    serializer_class = GameSerializer
    queryset = Game.objects.filter(private=False)

    def get(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class GameExists(mixins.ListModelMixin,
                 GenericAPIView):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(game_id=request.query_params['game_id'])
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
