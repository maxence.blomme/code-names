﻿from enum import Enum


class RequestTypeEnum(Enum):
    REVEAL = 1
    RESET = 2
    REGISTER_PLAYER = 3
    GET_PLAYERS = 4
    REMOVE_PLAYER = 5
    GET_CARDS = 6
    TOGGLE_SPY = 7
    GET_CLUES = 8
    ADD_CLUE = 9
    END_TURN = 10
