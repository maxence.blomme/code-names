from rest_framework import serializers
from .models import Word, Card, Player, Clue, Game


class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = ['word']


class ThemeSerializer(serializers.ModelSerializer):
    word_count = serializers.SerializerMethodField('get_word_count')

    def get_word_count(self, word):
        return len(Word.objects.filter(theme=word['theme']))

    class Meta:
        model = Word
        fields = ['theme', 'word_count']


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('game_id', 'word', 'color', 'revealed')


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ('game_id', 'name', 'color', 'is_admin', 'is_spy')


class ClueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clue
        fields = ('game_id', 'word', 'number', 'color')


class GameSerializer(serializers.ModelSerializer):
    players_amount = serializers.SerializerMethodField('get_players_amount')

    def get_players_amount(self, game):
        return len(Player.objects.filter(game_id=game.game_id).exclude(color="spec"))

    class Meta:
        model = Game
        fields = ('game_id', 'remaining_blue', 'remaining_red', 'state', 'blue_won', 'players_amount')


class CreateGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('game_id', 'private', 'theme', 'sec_theme')
